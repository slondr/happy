;;;; package.lisp

(in-package :cl-user)

(defpackage :happy
  (:use :cl)
  (:documentation "Determines if a given number is happy or sad")
  (:export #:happy #:pdi #:main))

