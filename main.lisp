;;; Main
;;; Entry point for the happy program
;;; Runs some tests and then returns the happiness of a user-inputted number


#|
      Happy
    Copyright (C) 2020  Eric S. Londres

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
|#

(in-package :happy)

(defun main ()
  "Entry point for program"
  (let ((known-happy-numbers '(1 7 13 19 23 31 79 97 103))
	(known-unhappy-numbers '(8 14 20 410 411 412 486)))
    (map nil (lambda (x) (assert (happy:happy x))) known-happy-numbers)
    (map nil (lambda (x) (assert (not (happy:happy x)))) known-unhappy-numbers))
  ;; Accept user input and print its happiness
  (format t "~A~%" (if (happy:happy (parse-integer (read-line))) "happy" "unhappy")))
