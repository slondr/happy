;; Happy
;; Determines if a given number is happy or not.
;; See https://en.wikipedia.org/wiki/Happy_number

#|
      Happy
    Copyright (C) 2020  Eric S. Londres

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
|#

(in-package :happy)

(defun pdi (number &optional (base 10) (total 0))
  "Computes the perfect digital invariant of the given number"
  (if (<= number 0)
      total
      (happy:pdi (floor number base) base (+ total (expt (mod number base) 2)))))

(defun happy (number &optional (slow (pdi number)) (fast (pdi (pdi number))))
  "Determine the happiness status of a number"
  (if (= slow fast)
      (= slow 1)
      (happy:happy number (happy:pdi slow) (happy:pdi (happy:pdi fast)))))
